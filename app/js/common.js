$(function() {

	$(window).scroll(function() {
		var scrollPercent = 100 * $(window).scrollTop() / ($(document).height() - $(window).height());
		$('.bar-long').css('width', scrollPercent +"%"  );
	});

});
